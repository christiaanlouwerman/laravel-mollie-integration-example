<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'customer_id' => ['required', 'integer'],
            'mollie_payment_id' => ['required'],
            'status' => ['required'],
            'method' => ['required'],
            'mollie_profile_id' => ['required'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}