<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MolliePaymentRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'amount' => ['required', 'array'],
            'amount.currency' => ['required', 'string'],
            'amount.value' => ['required', 'string'],
            'method' => ['required', 'string', 'max:20'],
            'description' => ['nullable', 'string'],
            'locale' => ['required', 'string'],
            'metadata' => ['array'],
            'array' => ['string']
        ];
    }

    protected function prepareForValidation()
    {
        return $this->merge([
            'array' => $this->locale
        ]);
    }

    public function authorize(): bool
    {
        return true;
    }
}