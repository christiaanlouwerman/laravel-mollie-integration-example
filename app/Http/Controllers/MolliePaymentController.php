<?php

namespace App\Http\Controllers;

use App\Http\Requests\MolliePaymentRequest;
use App\Models\Payment;
use App\Services\MollieService;
use App\Traits\ApiResponses;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Mollie\Api\Exceptions\ApiException;

class MolliePaymentController extends Controller
{
    use ApiResponses;

    protected MollieService $client;

    public function __construct(MollieService $client)
    {
        $this->client = $client;
    }

    /**
     * @throws ApiException
     */
    public function createPayment(MolliePaymentRequest $request)
    {
        $validated = $request->safe()->collect();

        $payment = Payment::create([
            'customer_id' => $request->user()->id,
            'amount' => $validated->value('value'),
        ]);

        return $this->client->preparePayment(
            $validated->value('currency'),
            $validated->value('value'),
            $validated->get('method'),
            $validated->get('description'),
            $validated->get('locale'),
            $payment->customer_id,
            $payment->id
        );
    }

    public function handleMollieRedirect($id): JsonResponse
    {
        if ($id) {
            $payment = $this->getPayment($id);

            try {
                return $this->handlePaymentStatus($payment);
            } catch (Exception $e) {
                return $this->handleError($e); // Handle API errors
            }
        }

        return $this->handleMissingPaymentId();
    }

    private function getPayment($id): Payment
    {
        return Payment::find($id);
    }

    /**
     * @throws ApiException
     */
    public function handlePaymentStatus(Payment $payment): JsonResponse
    {
        $molliePayment = $this->client->getMolliePayment($payment->mollie_payment_id);

        if (!$molliePayment->isPaid()) {
            return $this->ok('Payment ' . $payment->status);
        }

        return $this->success('Payment success', $payment);
    }

    private function handleError(Exception $e)
    {
        return $this->error('Error processing Mollie payment: ' . $e->getMessage(), 500);
    }

    private function handleMissingPaymentId()
    {
        return $this->error('Missing payment information,', 400);
    }

    /**
     * @throws ApiException
     */
    public function handleWebhook(Request $request): JsonResponse
    {
        if (!$request->input('id')) {
            return response()->json(['error' => 'Missing payment ID'], 400);
        }

        $molliePayment = $this->client->getMolliePayment($request->input('id'));
        $payment = $this->getPayment($molliePayment->metadata->paymentId);

        $payment->mollie_payment_id = $molliePayment->id;
        $payment->status = $molliePayment->status;
        $payment->method = $molliePayment->method;
        $payment->mollie_profile_id = $molliePayment->profileId;
        $payment->save();

        return $this->handlePaymentStatus($payment);
    }
}