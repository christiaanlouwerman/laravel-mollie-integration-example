<?php

namespace App\Http\Resources;

use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Payment */
class PaymentResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'type' => 'payment',
            'id' => $this->id,
            'attributes' => [
                'customer_id' => $this->customer_id,
                'status' => $this->status,
                'method' => $this->method,
                'mollie_payment_id' => $this->mollie_payment_id,
                'createdAt' => $this->when(
                    $request->routeIs('payments.show'),
                    $this->created_at
                ),
                'updatedAt' => $this->when(
                    $request->routeIs('payments.show'),
                    $this->updated_at
                )
            ],
            'relationships' => [
                'customer' => [
                    'data' => [
                        'type' => 'user',
                        'id' => $this->customer_id
                    ],
                    'links' => [
                        ['self' => 'todo']
                    ]
                ],
            ],
            'links' => [
                ['self' => route('payments.show', ['payment' => $this->id])]
            ],
        ];
    }
}