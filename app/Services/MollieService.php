<?php

namespace App\Services;


use App\Traits\ApiResponses;
use Mollie;
use Mollie\Api\Exceptions\ApiException;
use Mollie\Api\Resources\Payment;


class MollieService
{
    use ApiResponses;

    /**
     * @throws ApiException
     */
    public function preparePayment(
        string $currency,
        string $value,
        string $method,
        string $description,
        string $locale,
        int $customerId,
        int $paymentId,
    ): ?string {
        try {
            $payment = Mollie::api()->payments->create([
                'amount' => [
                    'currency' => $currency,
                    'value' => $value,
                ],
                'method' => $method,
                'description' => $description,
                'locale' => $locale,
                'metadata' => [
                    'customerId' => $customerId,
                    'paymentId' => $paymentId,
                ],
                'webhookUrl' => route('mollie.webhook'),
                'redirectUrl' => route('mollie.redirect', [
                    'id' => $paymentId
                ]),
            ]);
        } catch (ApiException $e) {
            return $this->error('Payment failed: ' . $e->getMessage(), 303);
        }

        $payment = Mollie::api()->payments->get($payment->id);

        return $payment->getCheckoutUrl();
    }

    /**
     * @throws ApiException
     */
    public function getMolliePayment(string $paymentId): Payment
    {
        return Mollie::api()->payments->get($paymentId);
    }

}