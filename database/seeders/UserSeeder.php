<?php

namespace Database\Seeders;

use App\Models\Payment;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run(): void
    {
        User::factory()->create([
            'name' => 'John Doe',
            'email' => 'customer@example.com',
        ]);

        User::factory(40)->create()->each(function ($user) {
            Payment::factory(rand(1, 6))->create([
                'customer_id' => $user->id
            ]);
        });
    }
}