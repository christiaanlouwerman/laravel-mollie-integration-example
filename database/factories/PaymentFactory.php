<?php

namespace Database\Factories;

use App\Models\Payment;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class PaymentFactory extends Factory
{
    protected $model = Payment::class;

    public function definition(): array
    {
        return [
            'customer_id' => User::inRandomOrder()->first()->id,
            'mollie_payment_id' => 'tr_' . $this->faker->unique()->randomNumber(8),
            'status' => $this->faker->randomElement(['paid', 'cancelled', 'expired', 'pending']),
            'method' => $this->faker->randomElement(['ideal', 'card', 'paypal']),
            'amount' => $this->faker->randomFloat(2, 1, 1000),
            'mollie_profile_id' => "pr_" . $this->faker->unique()->randomNumber(8),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}