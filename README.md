# Laravel Mollie Integration Example

## Description

This example demonstrates how to seamlessly integrate the `laravel-mollie` package into a Laravel project for handling payments and efficiently managing webhook responses.

## Features

1. **Login Functionality using Bearer Tokens:**
    - Implement secure authentication for users using Bearer tokens.

2. **Create Payments:**
    - Utilize the Mollie API to create payments.
    - Specify the payment amount, currency, method, and description.
    - Handle various payment scenarios (e.g., credit card, iDEAL, etc.).

3. **Webhook Response Handling:**
    - Set up a webhook endpoint to receive payment-related events from Mollie.
    - Extract relevant data from the webhook payload.

4. **Data Persistence:**
    - Save essential payment details (e.g., transaction ID, customer information) to your database.
    - Maintain a record of successful payments for auditing and reporting purposes.

5. **Dynamic Redirect URLs:**
    - Customize the redirect URL where Mollie sends customers after completing a payment.
    - Use data retrieved from the webhook to create personalized post-payment experiences.

6. **Retrieve Payments (GET Request):**
    - Implement an API endpoint to retrieve a list of all payments.
    - Provide an additional endpoint to fetch specific payment details by ID.

## Mollie API Key

Copy the ```env.example``` file to ```.env``` and insert your Mollie API Key into the ```MOLLIE_KEY``` variable.
```
MOLLIE_KEY=test_xxxxxxxxxxxxxxxxxx
```

## Authentication

 You can authenticate using the following credentials.

**email:** customer@example.com \
**password:** password

## Example Request
Use the following examples to make requests to the API.

#### Authenticate a User

```php
POST /api/login
```

#### Create a Payment

```php
POST /api/mollie/payment/create
```

Example of a POST request to create a payment:

```json
{
    "amount": {
        "currency": "EUR",
        "value": "100.00"
    },
    "method": "ideal",
    "description": "Test",
    "locale": "nl_NL"
}
```

#### Retrieve All Payments

```php
GET /api/payments
```

#### Retrieve a Payment Using the ID

```php
GET /api/payments/{id}
```