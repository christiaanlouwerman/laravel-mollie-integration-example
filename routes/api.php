<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\MolliePaymentController;
use App\Http\Controllers\PaymentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::post('login', [AuthController::class, 'login'])->name('login');

Route::middleware('auth:sanctum')->group(function () {
    Route::post('logout', [AuthController::class, 'logout'])->name('logout');
});

Route::apiResource('payments', PaymentController::class)->except('update', 'store');


Route::post('mollie/webhook', [MolliePaymentController::class, 'handleWebhook'])->name('mollie.webhook');
Route::get('mollie/redirect/{id}', [MolliePaymentController::class, 'handleMollieRedirect'])->name('mollie.redirect');

Route::post('mollie/payment/create', [MolliePaymentController::class, 'createPayment'])
    ->middleware('auth:sanctum')
    ->name('mollie.payment.create');